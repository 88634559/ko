fun main() {
    val authos = setOf("Shakespeare", "Hemingway", "Twain")
    val writers = setOf("Twain", "Shakespeare", "Hemingway")

    println(authos == writers)
    println(authos === writers)
}